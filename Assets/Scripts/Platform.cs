﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public Transform pos1;
    public Transform pos2;
    public float speed;

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, pos2.position, Time.deltaTime * speed);

        if(Vector3.Distance(transform.position, pos2.position) < 0.5f)
        {
            var tmp = pos2;
            pos2 = pos1;
            pos1 = tmp;
        }
    }
}
