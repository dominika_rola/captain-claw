using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;
    private bool canJump = true;
    [SerializeField] private float jumpForce;

    private int scores = 0;
    [SerializeField] private TMP_Text scoreText;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("move", true);
        }
        else
        {
            anim.SetBool("move", false);
        }
        
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(new Vector2(speed, 0));
            sr.flipX = false;
        }
        
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(new Vector2(-speed, 0));
            sr.flipX = true;
        }

        if (Input.GetKey(KeyCode.Space) && canJump == true)
        {
            rb.AddForce(new Vector2(0, jumpForce));
            anim.SetTrigger("jump");
            canJump = false;
        }

        if (Physics2D.Raycast(transform.position, Vector3.down, 0.6f, 1 << 8))
        {
            canJump = true;
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetTrigger("attack");
            if (sr.flipX)
            {
                //right
                RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y + 0.5f), Vector3.left, 2, 1<<9);

                if(hit)
                {
                    Destroy(hit.transform.gameObject);
                }
                
            }
            else
            {
                //left
                RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y + 0.5f), Vector3.right, 2, 1<<9);

                if(hit)
                {
                    Destroy(hit.transform.gameObject);
                }
                    
            }
                
            
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Coin")
        {
            scores += 1;
            scoreText.text = "Scores: " + scores.ToString();
            Destroy(collision.gameObject);
            
        }
        
        if (collision.tag == "WinCoin")
        {

            SceneManager.LoadScene("Win");
        }
        
        if (collision.tag == "LoseCoin")
        {
            
            SceneManager.LoadScene("Lost");
        }
    }
}
 